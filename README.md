# Énigmes algorithmiques

Découverte des bases de l'algorithmique en langage naturel par le biais d'énigmes réalisées dans le navigateur.

Résoudre une énigme permet d'accéder à la suivante en utilisant la réponse comme nouvelle URL.

Site accessible sur https://nreveret.forge.apps.education.fr/enigmes