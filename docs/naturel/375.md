---
title: Énigme 9
author: N. Revéret
hide:
  - footer
---

# Énigme `9`

Bravo ! Vous l'avez compris, il faut bien faire attention aux bornes de l'ensemble à parcourir : sont-elles incluses ou non ?

En parlant d'ensembles, les boucles « Pour » permettent aussi de parcourir des ensembles quelconques. Voyez ci-dessous.

??? question "Le prix total"

    On considère le ticket de caisse ci-dessous :
    
    |      Article      | Prix  en euros |
    | :---------------: | :------------: |
    |      Pommes       |      3,4       |
    | Produit vaisselle |      3,6       |
    |    Essuie-tout    |       2        |
    |      Tomates      |       4        |
    |      Ananas       |      3,1       |

    Quelle est la valeur affichée à l'issue de cet algorithme ?
    
    ```texte
    prix_comestible = 0
    Pour chaque article du ticket de courses :
        Si cet article est comestible :
            Ajouter le prix de l'article à prix_comestible
    Afficher prix_comestible en centimes
    ```
    
    {{ terminal() }}
    