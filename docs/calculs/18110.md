---
title: Énigme 6
author: N. Revéret
hide:
  - footer
---

# Énigme `6`

Bravo, ce n'était pas immédiat et sans doute un peu long...

Vous savez donc chiffrer un message à l'aide du codage affine. Mais pour déchiffrer ? On fait l'inverse !

???+ note "Décodage affine"

    Supposons que l'on ait codé un message en utilisant les nombres `a` et `b`.  Pour déchiffrer un code `y`, on doit calculer le reste de `(y - b) * a_prime` par `#!py 26`.

    Comment calculer `a_prime` ? Lorsque c'est possible c'est **l'unique entier compris entre `#!py 2` et `#!py 25` tel que le reste de `a * a_prime` par `#!py 26` soit égal à `#!py 1`**.

    Si l'on prend `#!py a = 17`, alors `#!py a_prime = 23`. En effet :

    ```pycon
    >>> (17 * 23) % 26
    1
    ```

    Tous les autres essais, `#!py (17 * 1) % 26`, `#!py (17 * 2) % 26`, ..., `#!py (17 * 25) % 26`, donnent un résultat **différent de 1**.

    Bilan des courses :
    
    * si l'on prend `#!py a = 17`, `#!py b = 6` ;
  
    * on détermine que `#!py a_prime = 23`
  
    * pour décoder `#!py y = 25`, on calcule le reste de `#!py (y - b) * a_prime = (25 - 6) * 23` par `#!py 26`. On obtient `#!py 21` ;
  
    * la lettre de rang `#!py 21` dans l'alphabet est le `V`.

??? question "Énigme"

    On a pris `#!py a = 7` et `#!py b = 16` et l'on a obtenu le chiffre suivant : `#!py 19 10 17`.

    Quel était le message initial ? Saisir votre réponse en minuscule.

    {{ terminal() }}


??? tip "Aide"

    Vous devez déjà trouver `a_prime`... Comment ? Comme dit dans le texte, c'est un nombre entier entre `#!py 2` et `#!py 25`.

    Un coup de pouce en plus : c'est un nombre [premier avec `#!py 26`](https://www.dcode.fr/nombres-premiers-entre-eux){ target="_blank"}.

    | Lettre | Rang `x`  |
    | :----: | :-------: |
    |  `A`   | `#!py 0`  |
    |  `B`   | `#!py 1`  |
    |  `C`   | `#!py 2`  |
    |  `D`   | `#!py 3`  |
    |  `E`   | `#!py 4`  |
    |  `F`   | `#!py 5`  |
    |  `G`   | `#!py 6`  |
    |  `H`   | `#!py 7`  |
    |  `I`   | `#!py 8`  |
    |  `J`   | `#!py 9`  |
    |  `K`   | `#!py 10` |
    |  `L`   | `#!py 11` |
    |  `M`   | `#!py 12` |
    |  `N`   | `#!py 13` |
    |  `O`   | `#!py 14` |
    |  `P`   | `#!py 15` |
    |  `Q`   | `#!py 16` |
    |  `R`   | `#!py 17` |
    |  `S`   | `#!py 18` |
    |  `T`   | `#!py 19` |
    |  `U`   | `#!py 20` |
    |  `V`   | `#!py 21` |
    |  `W`   | `#!py 22` |
    |  `X`   | `#!py 23` |
    |  `Y`   | `#!py 24` |
    |  `Z`   | `#!py 25` |





