---
title: Fin
author: N. Revéret
hide:
  - footer
---

# Fin du parcours

Félicitations, vous avez terminé le parcours « Bases de calculs ».

Vous pouvez choisir un nouveau parcours. Pourquoi pas [« **Le langage naturel** »](../naturel/1.md) ?




