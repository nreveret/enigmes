---
author: Nicolas Revéret
title: Crédits
---

Site créé par Nicolas Revéret (`nreveret<à>ac-rennes<point>fr`)

Le site est construit avec [`mkdocs`](https://www.mkdocs.org/) et en particulier [`mkdocs-material`](https://squidfunk.github.io/mkdocs-material/).

Le logo utilisé provient de <a href="https://www.flaticon.com/free-icons/uncertainty" title="uncertainty icons">Uncertainty icons created by Uniconlabs - Flaticon</a>.