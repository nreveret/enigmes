# Calculs

## Énigme `1` 1 -> 4

Vous avez relevé le défi. Débutons donc avec une énigme pas très compliquée, juste pour comprendre le fonctionnement du site !

??? question "Première énigme - Déroulez-moi !"

    Combien font `#!py 2 + 2` ?

    On l'a dit, cette énigme n'est là que pour comprendre le fonctionnement du site. Rassurez-vous cela va se compliquer !


??? tip "Comment vérifier ma réponse ?"

    Vous pensez que la bonne réponse est `#!py 4` ? :wink:

    Pour le vérifier, vous n'avez qu'à modifier l'adresse de la page en remplaçant `depart` par `#!py 4`.

    Pour jouer vous pouvez aussi essayer de répondre `#!py 5` !


## Énigme `2` 4 -> 33

Vous y êtes arrivé. Bravo.

Au fait, ce site n'enregistre aucune donnée vous concernant. La contrepartie c'est qu'il n'y a pas de sauvegarde... Mais rassurez-vous, si vous voulez directement retourner sur la page d'une énigme, il vous suffit de copier l'adresse de celle-ci dans la barre d'adresse.

Si par exemple vous voulez directement revenir à cette page la prochaine fois, il vous suffit de copier/coller l'adresse dans un bloc-note. Lors de votre prochaine session, vous n'aurez qu'à coller cette adresse dans la barre d'adresse.

C'est clair ? On continue avec une nouvelle énigme.


??? question "Opérateurs"

    Nous allons considérer les opérations classiques sur les nombres mais dans leur « écriture informatique » :

    * `+` pour l'addition ;
    * `-` pour la soustraction ;
    * `*` pour la multiplication ;
    * `/` pour la division ;
    * `**` pour la puissance.

    Bien entendu, les règles de priorités son conservées et l'on peut utiliser des parenthèses.

    Combien fait `#!py (12345 + 75 * 7) - ((5**6 - 6**4) - 4 * (4 * 75 + 73))` ?


??? tip "Aide"

    Vous avez le droit à la calculatrice !


## Énigme `3` 33 -> 27990

Bravo. Vous avez fait cela de tête ?

Au fait, nous allons désormais utiliser un genre de calculatrice intégrée. Il s'agit plutôt d'un terminal `Python` mais nous y reviendrons plus tard.

Vous pouvez taper des calculs dans le terminal. Pressez ensuite « Entrée » et le calcul s'effectue (s'il n'y a pas de faute !).

{{ terminal() }}


??? question "Énigme"

    Nous allons ajouter deux opérateurs :

    * `//` permet de calculer le quotient (entier) de deux nombres ;
    * `%` permet de calculer le reste de la division euclidienne de deux nombres.

    Vous pouvez consulter [cette page](https://nreveret.forge.apps.education.fr/preuves_visuelles/#division-euclidienne) qui illustre la division euclidienne. En bref, sachant que $23 = 4 \times 5 + 2$ :

    ```pycon
    >>> 23 // 5  ## le quotient de 23 par 5
    4
    >>> 23 % 5  ## le reste de 23 par 5
    3
    ```

    Combien vaut le quotient de `#!py 12657` par `#!py 2135` multiplié au reste de `#!py 165798` par `#!py 6675` ?

??? tip "Aide"

    Utilisez le terminal et **n'oubliez-pas les parenthèses !**

## Énigme `4` 27990 -> 19162363119

Vous connaissez désormais les 6 opérateurs de calculs : 

| Addition | Soustraction | Multiplication | Division | Quotient | Reste |
| :------: | :----------: | :------------: | :------: | :------: | :---: |
|   `+`    |     `-`      |      `*`       |   `/`    |   `//`   |  `%`  |


Mettons les tous en œuvre.

??? question "Énigme 🦆🐈🐄"

    Dans une grande ferme, il n'y a que des animaux bien portants à deux pattes (la volaille) ou à quatre pattes (le bétail).

    Il y a au total 593 animaux dont 228 à deux pattes.

    Parmi les 593 animaux, il y a trois chats mâles et 7 chattes. Chaque chatte a 7 chatons.

    En une année, l'agricultrice récupère 3 729 œufs. Afin de les vendre, elle les range dans des boîtes de 12 œufs.

    Répondre au questions suivantes :

    1. Il y a 593 animaux dans la ferme. Combien de pattes cela représente au total ?
    
    2. Il y a beaucoup de chats dans cette ferme. Combien de pattes ont-ils au total ?
    
    3. Combien de boîtes d'œufs l'agricultrice utilise-t-elle en un an ?
    
    4. Une boîte n'est pas entièrement remplie. Combien compte-t-elle d'œufs ?

    Écrire les réponses à chaque question à la suite les unes des autres afin de trouver l'adresse de la prochaine page.
    
    Par exemple, les réponses `#!py 123`, `#!py 45`, `#!py 678` et `#!py 9` créent l'adresse `#!py 123456789`.

    {{ terminal() }}

??? tip "Aide"

    Avez-vous bien pensé à compter tous les animaux ? À deux pattes et à quatre pattes ?

    Avez-vous bien pensé à compter tous les chats ? Mâles, femelles, chatons ?

    Avez-vous bien pensé à compter toutes les boîtes ? Y compris la dernière ?

## Énigme `5` 19162363119 --> 18110

Nous sommes désormais équipés pour faire des calculs. :+1:

Au fait, connaissez-vous le [codage affine](https://fr.wikipedia.org/wiki/Chiffre_affine) ? Il s'agit d'une méthode de chiffrement reposant sur des calculs « simple ».

???+ note "Codage affine"

    Afin de chiffrer un message :

    * on choisit tout d'abord deux nombres `a` et `b`. Nous prendrons `#!py a = 9` et `#!py b = 20` ;

    * on remplace ensuite chaque lettre à chiffrer par son rang dans l'alphabet : `A` devient `#!py 0`, `B` devient `#!py 1`, ..., `Z` devient `#!py 25` ;

    * si `x` est le rang d'une lettre dans l'alphabet, sa version *chiffrée* est le reste de `a * x + b` par `#!py 26`.

Ce n'est pas clair ? Faisons un exemple, toujours avec `#!py a = 9` et `#!py b = 20`. Chiffrons le message `ABC` :

* Le rang de `A` est `#!py 0` ; `#!py 9 * 0 + 20` vaut `#!py 20` et le reste de `#!py 20` par `#!py 26` vaut `#!py 20` ;

* Le rang de `B` est `#!py 1` ; `#!py 9 * 1 + 20` vaut `#!py 29` et le reste de `#!py 29` par `#!py 26` vaut `#!py 3` ;

* Le rang de `C` est `#!py 2` ; `#!py 9 * 2 + 20` vaut `#!py 38` et le reste de `#!py 38` par `#!py 26` vaut `#!py 12`.

En écrivant à la suite le code associé à chaque lettre on obtient `#!py 20312`.

??? question "Énigme"

    Quel est le nombre chiffrant le mot `PYTHON` ? On conserve `#!py a = 9` et `#!py b = 20`.

    {{ terminal() }}

## Énigme `6` 18110 --> top (fin)

Bravo, ce n'était pas immédiat et sans doute un peu long...

Vous savez donc chiffrer un message à l'aide du codage affine. Mais pour déchiffrer ? On fait l'inverse !

???+ note "Décodage affine"

    Supposons que l'on ait codé un message en utilisant les nombres `a` et `b`.  Pour déchiffrer un code `y`, on doit calculer le reste de `(y - b) * a_prime` par `#!py 26`.

    Comment calculer `a_prime` ? Lorsque c'est possible c'est **l'unique entier compris entre `#!py 2` et `#!py 25` tel que le reste de `a * a_prime` par `#!py 26` soit égal à `#!py 1`**.

    Si l'on prend `#!py a = 17`, alors `#!py a_prime = 23`. En effet :

    ```pycon
    >>> (17 * 23) % 26
    1
    ```

    Tous les autres essais, `#!py (17 * 1) % 26`, `#!py (17 * 2) % 26`, ..., `#!py (17 * 25) % 26`, donnent un résultat **différent de 1**.

    Bilan des courses :
    
    * si l'on prend `#!py a = 17`, `#!py b = 6` ;
  
    * on détermine que `#!py a_prime = 23`
  
    * pour décoder `#!py y = 25`, on calcule le reste de `#!py (y - b) * a_prime = (25 - 6) * 23` par `#!py 26`. On obtient `#!py 21` ;
  
    * la lettre de rang `#!py 21` dans l'alphabet est le `V`.

??? question "Énigme"

    On a pris `#!py a = 7` et `#!py b = 16` et l'on a obtenu le chiffre suivant : `#!py 19 10 17` (les codes sont séparés par des espaces).

    Quel était le message initial ? Saisir votre réponse en minuscule.

    {{ terminal() }}


??? tip "Aide"

    Vous devez déjà trouver `a_prime`... Comment ? Comme dit dans le texte, c'est un nombre entier entre `#!py 2` et `#!py 25`.

    Un coup de pouce en plus : c'est un nombre [premier avec `#!py 26`](https://www.dcode.fr/nombres-premiers-entre-eux){ target="_blank"}.

    | Lettre | Rang `x`  |
    | :----: | :-------: |
    |  `A`   | `#!py 0`  |
    |  `B`   | `#!py 1`  |
    |  `C`   | `#!py 2`  |
    |  `D`   | `#!py 3`  |
    |  `E`   | `#!py 4`  |
    |  `F`   | `#!py 5`  |
    |  `G`   | `#!py 6`  |
    |  `H`   | `#!py 7`  |
    |  `I`   | `#!py 8`  |
    |  `J`   | `#!py 9`  |
    |  `K`   | `#!py 10` |
    |  `L`   | `#!py 11` |
    |  `M`   | `#!py 12` |
    |  `N`   | `#!py 13` |
    |  `O`   | `#!py 14` |
    |  `P`   | `#!py 15` |
    |  `Q`   | `#!py 16` |
    |  `R`   | `#!py 17` |
    |  `S`   | `#!py 18` |
    |  `T`   | `#!py 19` |
    |  `U`   | `#!py 20` |
    |  `V`   | `#!py 21` |
    |  `W`   | `#!py 22` |
    |  `X`   | `#!py 23` |
    |  `Y`   | `#!py 24` |
    |  `Z`   | `#!py 25` |
    
# Naturel